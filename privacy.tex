\documentclass{article}

\usepackage[ruled,vlined]{algorithm2e}
\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,arrows}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth


\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\Lap}{Lap}
\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\vect}{vec}
\DeclareMathOperator{\logdet}{log\;det}
\newcommand{\tA}{\tilde A}
\newcommand{\tC}{\tilde C}
\newcommand{\tP}{\tilde P}


\title{privacy}

\begin{document} \maketitle


This chapter consider the privacy aspect of Cyber Physical Systems. For large scale CPS such as power grid, there are usually multiple actors involved in its operation. For example, the successful operation of the power grid requires the participation of generators, carriers and distributors, service providers, customers, and operators \cite{Fang2012}. However, each participant may have different interest and therefore do not want to reveal more information than necessary to other players. For example, it is well known that fine-grained power usage data, which can be used for demand response purpose, can also reveal information about the presence, absence and even specific activities of a home's occupants \cite{Cortes2016}. One way to prevent privacy breach is to develop a privacy mechanism to pre-process sensitive data before they are released in order to preserve the privacy of the participants. Therefore, a metric on the privacy guarantee of the mechanism needs to be developed.

For the rest of the chapter, we first consider the notion of data privacy, with an emphasis on the differential privacy. We then consider an example of average consensus and design differentially private mechanism to guarantee the privacy of the initial conditions of the consensus. Due to the limitation of the differential privacy mechanism, e.g., it cannot achieves the exact average consensus, we consider a different concept inference privacy and propose inferentially private mechanism that can achieve exact average consensus. Finally, we give a brief introduction on cryptography based privacy.

\section{Data Privacy}

In this section, we consider data privacy mechanisms. Suppose that there is a data set $d\in \mathbb R^n$ and we would like to publish certain function of the data set $q(d)$. However, as directly publishing $q(d)$ may cause privacy breach, we need to distort $q(d)$ before releasing it.

A privacy mechanism is thus defined as a probabilistic mapping $u = M(d)$, which is characterized by a transition probability $p_{u|d}$. In other words, given $d$, $u$, which is a distorted version of $q(d)$, follows the distribution $p_{u|d}$.

Differential privacy is a notion to quantify the privacy guarantees provided by the probabilistic mapping $M$. To define differential privacy, we first need to establish an adjacency relation on the data set, which characterizes how close two data sets are. In general, any symmetric relation can be defined as an adjacency relation. For data set, usually the following definition of $\delta$-adjacency is used:
\begin{definition}
 Two data sets $d$ and $d'$ are called $\delta$-adjacent if they differ at no more than one entry and the difference is no greater than $\delta$, i.e., 
 \begin{align*}
   \|d-d'\|_0\leq 1,\,\|d-d'\|_1 \leq \delta.
 \end{align*}
\end{definition}
Notice that other definitions of adjacency exist. For example, we can define $d$ and $d'$ are adjacent if $\|d-d'\|_p\leq \delta$. However, we only focus on $\delta$-adjacency in this chapter.

A mechanism $M$ is $\epsilon$-differential private if for any two adjacent data set $d$ and $d'$, and any measureable set $\mathcal R$, the following inequality holds:
\begin{align*}
 P(u = M(d) \in  \mathcal R)\leq \exp(\epsilon)P(u=M(d')\in \mathcal R).
\end{align*}

It is worth noticing that other metrics for data privacy exist. For example, $\epsilon$-identifiability ensures that for all neighboring $d$ and $d'$,
\begin{align*}
 \frac{P(d|u)}{P(d'|u)}\leq \exp(\epsilon). 
\end{align*}
$\epsilon$-mutual information privacy guarantees that the mutual information between $d$ and $u$ is less than $\epsilon$. However, both identifiability and mutual information privacy require the prior distribution of the data base, while differential privacy does not. For more detailed discussions on other privacy metrics, please refer to \cite{Sun2017}.

As an example of differential privacy, let us consider a data base $d\in \mathbb R^n$ with $n$ entries, such that entry $i$ represents the monthly power consumption of household $i$. We would like to publish the average power consumption $q(d) = \mathbf 1^Td/n$.

To define our privacy mechanism, we call a random variable $w$ follows a Laplacian distribution with parameter $b$ if its probability density function satisfies:
\begin{align*}
  f(w) =\frac{1}{2b}\exp\left(-\frac{|w|}{b}\right).
\end{align*}
We will write $w\sim \Lap(b)$ for short.

In order to ensure $\epsilon$-differential privacy, we can choose our mechanism to be
\begin{align}
  u = M(d) = q(d)+w = \mathbf 1^Td/n + w,
  \label{eq:laplace}
\end{align}
where $w \sim \Lap(b)$, with $b=\delta/(n\epsilon)$. This mechanism is  $\epsilon$-differentially private, since for any two $\delta$-adjacent data set $d$ and $d'$
\begin{align*}
 P(M(d)\in \mathcal R) &= \int_{\mathcal R} \frac{1}{2b}\exp\left(-\frac{|u-\mathbf 1^Td/n|}{b}\right) du  \\
&\leq \exp\left(-\frac{|\mathbf 1^T(d-d')|}{nb}\right)\int_{\mathcal R}\frac{1}{2b}\exp\left(-\frac{|u-\mathbf 1^Td'/n|}{b}\right) du \\
&\leq \exp(\epsilon)P(M(d')\in \mathcal R).
\end{align*}
The probability density function of $u$ is illustrated in Fig~\ref{fig:laplacianmechanism}. It is worth noticing that a smaller $\epsilon$ will result in better privacy, since $M(d)$ and $M(d')$ will be hardly distinguishable. However, a larger $\epsilon$ also increases the variance of the noise $w$, which deteriorates the data. Hence, it is important to balance the trade-off between privacy and the utility of the data by choosing an appropriate privacy budget $\epsilon$.
\begin{figure}[ht]
  \centering
  \begin{tikzpicture}
    \draw [-stealth] (-1.5,0)--(5,0) node [anchor=90] {$u$};
    \draw [-stealth] (0,-0.5)--(0,3.5) node [anchor=-90] {$p(u|d)$};
  \draw [dashed] (1,3)--(1,0) node[anchor=90] {$\frac{\mathbf 1^Td}{n}$};
  \draw [dashed] (2.5,3)--(2.5,0) node[anchor=90] {$\frac{\mathbf 1^Td'}{n}$};
  \draw[domain=-2:2, samples=200,smooth,variable=\t,blue,thick] plot ({\t+1},{3*exp(-1*abs(\t))});
  \draw[domain=-2:2, samples=200,smooth,variable=\t,blue,dashdotted,thick] plot ({\t+2.5},{3*exp(-1*abs(\t))});
  \end{tikzpicture}
  \caption{The probability density function of the output of the Laplacian mechanism $M(d)$ defined in \eqref{eq:laplace}.\label{fig:laplacianmechanism} }
\end{figure}

One of the main advantage of $\epsilon$-differential privacy is the resiliency to post-processing of the data, which is proved by the following theorem:
\begin{theorem}[Post-processing~\cite{Cortes2016}]
 Suppose that a mechanism $M$ preserves $\epsilon$-differential privacy, then for any function $f$, the functional composition $f\circ M$ also preserves $\epsilon$-differential privacy.
\end{theorem}
\begin{proof}
 For any set $\mathcal R$, the event $f\circ M(d)\in \mathcal R$ is equivalent to $M(d)\in f^{-1}(\mathcal R)$, where $f^{-1}(\mathcal R)$ is the preimage of $\mathcal R$ under $f$ defined as
\begin{align*}
  f^{-1}(\mathcal R) =\left\{x: f(x)\in \mathcal R\right\}.
\end{align*}
As a result, for any adjacent $d$ and $d'$, we have
\begin{align*}
  P(f\circ M (d)\in \mathcal R) &= P(M(d)\in f^{-1}(\mathcal R))\\
 &\leq \exp(\epsilon) P(M(d')\in f^{-1}(\mathcal R)) = \exp(\epsilon)P(f\circ M(d')\in \mathcal R),
\end{align*}
which finishes the proof.
\end{proof}

However, the post-processing theorem can be too restrictive for certain applications as it forbids ``accurate'' inference of any function of $d$ from $u$. In the next subsection, we consider the problem of average consensus and show that the concept of differential privacy may be too strong for certain applications.

%To this end, we assume that there are certain private information $s$ that we need to keep secret and $s$ and $d$ has a joint probability distribution. Inference privacy ensures that $s$ cannot be inferred accurately from the released data $u$. However, other functions of $d$ may still be inferred. The following metrics are proposed in the literature citesun17 for inference privacy:
%\begin{definition}
%  A mechanism $M$ is $\epsilon$-information private if for any $s$ and $u$, the following inequalities hold:
%  \begin{align*}
%    \exp(-\epsilon)\leq \frac{p(s|u)}{p(s)}\leq \exp(\epsilon).
%  \end{align*}
%\end{definition}
%
%\begin{definition}
%  A mechanism $M$ achieve $\epsilon$-average information leakage if the following inequalities hold:
%  \begin{align*}
%    I(s;u)\leq \epsilon,
%  \end{align*}
%  where $I(s;u)$ is the mutual information between $s$ and $u$.
%\end{definition}
%
%In the next section, we consider an example of average consensus, by applying both data privacy mechanism and inference privacy mechanism.

\section{Examples: Average Consensus}

To illustrate the various privacy mechanisms, we use the example of average consensus, where a group of agents, each with an initial value, wants to compute the average of their initial values. However, the agents would like to do the consensus in a privacy preserving way, in the sense that their initial values are not revealed to the other agents. Privacy preserving consensus can be used in power grids, where the smart meters can locally fuse the power consumption data and report to the central controller only the aggregated data.

For the rest of the section, we first give a brief introduction on the consensus problem and then discuss the differential privacy approach. A weaker notion of privacy, namely inference privacy, is introduced later to deal with the limitations of differential privacy.

\subsection{Average Consensus}
We model a network composed of $n$ agents as a graph $G = \{V,\,E\}$. $V = \{1,2,\ldots,n\}$ is the set of vertices representing the agents. $E \subseteq V\times V$ is the set of edges. $(i,j)\in E$ if and only if agent $i$ and $j$ can communicate directly with each other.  The neighborhood of agent $i$ is defined as
\begin{displaymath}
  \mathcal N(i) \triangleq \{j\in V:(i,j)\in E,\,j\neq i\}. 
\end{displaymath}
Suppose that each agent has an initial scalar state $x_{0,i}$. At each iteration, agent $i$ will communicate with its neighbors and update its state according to the following equation:
\begin{equation}
  x_{k+1,i} = a_{ii} x_{k,i} + \sum_{j\in \mathcal N(i)} a_{ij} x_{k,j}.  
  \label{eq:singleupdate}
\end{equation}
Define $x_k \triangleq [x_{k,1},\ldots,x_{k,n}]^T\in \mathbb R^n$ and $A \triangleq \left[a_{ij}\right] \in \mathbb R^{n\times n}$. The update equation \eqref{eq:singleupdate} can be written in matrix form as
\begin{equation}
  x_{k+1}= A x_k.  
  \label{eq:matrixupdate}
\end{equation}
Furthermore, define the average vector to be
\begin{displaymath}
  \bar x\triangleq \frac{\mathbf 1^T x_0}{n} \mathbf 1.
\end{displaymath}
We say the (asymptotic) consensus is achieved if $x_{k,i} -  x_{k,j}$ converges to $0$ for any pair of $i,\,j$, i.e., the difference between the states of any agent $i$ and $j$ converges to $0$. The accuracy of the convergence is defined as
\begin{align*}
\lim_{k\rightarrow\infty}E\|x_k - \bar x\|_2^2. 
\end{align*}

The consensus is exact if $x_k - \bar x$ converges to $0$. In other words, the state of each agent converges to the exact average of the initial condition. 

If we arrange the eigenvalues of $A$ in the decreasing order as $|\lambda_1|\geq |\lambda_2|\ldots\geq |\lambda_n|$. It is well known that the following conditions are necessary and sufficient in order to achieve average consensus from any initial condition $x(0)$:
\begin{enumerate}
\item[(A1)] $ \lambda_1 = 1$ and  $|\lambda_i| < 1$ for all $i = 2,\ldots, n$.
\item[(A2)] $A\mathbf 1 = \mathbf 1$, and $\mathbf 1^TA =\mathbf 1^T$ , i.e., $\mathbf 1$ is both an left and right eigenvector of $A$.
\end{enumerate}


\subsection{Differential Privacy}

We propose the following consensus algorithm in order the preserve the privacy of each agent:

\begin{algorithm}
\SetAlgoLined
  \begin{enumerate}
  \item At time $k$, each agent adds a random noise $w_{k,i}$ to its state $x_{k,i}$. Define the new state to be $x_{k,i}^+$, i.e.,
    \begin{equation}
      x_{k,i}^+ = x_{k,i} + w_{k,i}.  
      \label{eq:noisestep}
    \end{equation}
  \item Each agent then communicates with its neighbors and update its state to the average value by the following time invariant equation:
    \begin{equation}
      x_{k+1,i} = a_{ii}x_{k,i}^++\sum_{j\in \mathcal N(i)}a_{ij}x_{k,j}^+.
      \label{eq:consensusstep}
    \end{equation}
  \item Advance the time to $k+1$ and go to step 1).
  \end{enumerate}
 \caption{Privacy Preserving Average Consensus\label{algorithm:aveconsensus}}
\end{algorithm}

Define 
\begin{align}
  w_k &\triangleq [w_{k,1},\ldots,w_{k,n}]^T\in \mathbb R^n,\, x_k^+ \triangleq [x^+_{k,1},\ldots,x^+_{k,n}]^T\in \mathbb R^n. 
\end{align}
We can write \eqref{eq:noisestep} and \eqref{eq:consensusstep} in matrix form as
\begin{equation}
  x_{k+1} =Ax^+(k)= A(x_k + w_k).
  \label{eq:matrixform}
\end{equation}

Let us define the adjacency relationship on the initial conditions:
\begin{definition}
$x_0$ and $x_0'$ are $\delta$-adjacent if only one entry of $x_0'$ is different from $x_0$, and the difference is no greater than $\delta$.
\end{definition}

Let us denote the infinite sequence $x^+$ as $(x^+_0,x^+_1,\ldots)$. Clearly $x^+$ is a function of both the noise sequence $w = (w_0,\,w_1,\ldots)$ and the initial condition $x_0$. Hence, we could write it as $x^+(x_0,w)$. To ensure the $\epsilon$-differential privacy, one would need to design the noise sequence $w$, such that
\begin{align}
 P(x^+(x_0,w)\in \mathcal R) \leq \exp(\epsilon)P(x^+(x_0',w)\in \mathcal R),
  \label{eq:differentialprivateconsensus}
\end{align}
for any measurable set $\mathcal R$ and any $\delta$-adjacent initial condition $x_0$ and $x_0'$.

A simple way to ensure $\epsilon$-differential privacy is to choose $w_{0,i}$ to be independent noise following Laplacian distribution $\Lap(\delta/\epsilon)$, with all the future $w_{k,i} = 0$. Essentially it is equivalent to perturbing the initial state $x_0$ and doing the standard average consensus on the perturbed initial state. To prove the mechanism is indeed $\epsilon$-differentially private, for any $x_0$ and $x_0'$ that are $\delta$-adjacent, we have
\begin{align*}
  P(x_0^+=x_0+w_0\in \mathcal R)\leq \exp(\epsilon)P(x_0^+=x_0'+w_0\in \mathcal R).
\end{align*}
Furthermore, since all the future $x_k^+$ are functions of $x_0^+$, \eqref{eq:differentialprivateconsensus} holds by the post-processing theorem.

Since $x_k$ will converges to $\bar x + \mathbf 1^T w_0/n\times \mathbf 1$, the accuracy of this approach is given by
\begin{align*}
  \mathbb E \left\|\frac{\mathbf 1\mathbf 1^n}{n}w_0\right\|_2^2 =   \frac{1}{n}\mathbb E(\mathbf 1^T w_0)^2 = 2\delta^2/\epsilon^2.
\end{align*}

Notice that this adding one-shot noise scheme is optimal in the sense that it provide the best accuracy within certain privacy requirement. For more details, please refer to \cite{Nozari2017}.

We further provide an impossible result on the differentially private average consensus:
\begin{theorem}
Suppose that the consensus algorithm is $\epsilon$-differential privacy, i.e., \eqref{eq:differentialprivateconsensus} holds, then $x_k$ cannot converge to $\bar x$ in probability for all initial condition $x_0$.
\label{theorem:impossible}
\end{theorem}
\begin{proof}
  From \eqref{eq:matrixform}, $x_k$ converges to $\bar x$ in probability only if $x_k^+$ converges to $\bar x$ in probability, which is implies that for any $\tau > 0$:
\begin{align*}
 \lim_{k\rightarrow\infty} P(\|x_k^+-\bar x\|_1<\tau) = 1.
\end{align*}

Now suppose we choose $x_0$ and $x_0'$ that are $\delta$-adjacent to each other. Let us choose $\tau = \|x_0-x_0'\|_1/2$. By weak convergence, we have
\begin{align*}
 \lim_{k\rightarrow\infty} P(\|x_k^+(x_0,w)-\mathbf 1^T x_0/n\|_1<\tau) = 1.
\end{align*}
Now from $\epsilon$-differential privacy
\begin{align*}
  P(\|x_k^+(x_0',w)-\mathbf 1^T x_0/n\|_1<\tau) \geq \exp(-\epsilon) P(\|x_k^+(x_0,w)-\mathbf 1^T x_0/n\|_1<\tau),
\end{align*}
which implies that
\begin{align*}
 \liminf_{k\rightarrow\infty} P(\|x_k^+(x_0',w)-\mathbf 1^T x_0/n\|_1<\tau) \geq \exp(-\epsilon).
\end{align*}
However, this is impossible since
\begin{align*}
  P(\|x_k^+(x_0',w)-\mathbf 1^T x_0/n\|_1<\tau) \leq 1 - P(\|x_k^+(x_0',w)-\mathbf 1^T x_0'/n\|_1<\tau),
\end{align*}
the RHS of which converges to $0$ at $k$ goes to infinity.
\end{proof}

Notice that since $L_p$ convergence and almost surely convergence both imply convergence in probability, $x_k$ cannot converge to $\bar x$ in $L_p$ sense nor almost surely. Hence, it is impossible to achieve differential privacy while achieving exact consensus. Roughly speaking, this is due to the fact that the privacy guarantee provided by differential privacy is too strong. In the next subsection, we consider a ``weaker'' inference privacy guarantee and prove that we can achieve exact average consensus while preserving inference privacy.

\subsection{Inference Privacy}

In this section, we propose a mechanism to achieve exact average consensus. Notice that such a mechanism cannot be differentially private due to Theorem~\ref{theorem:impossible}. Therefore, we need to develop a different privacy metric to quantify the privacy guarantees of the mechanism. We want to ensure that any agent participating in the consensus cannot infer the initial conditions of other agents, which is called ``inference privacy''. Throughout this subsection, we make an additional assumption that $A$ is symmetric.

 We will still leverage Algorithm~\ref{algorithm:aveconsensus}, but with a different noise process $\{w_k\}$. To this end, let us define $v_{k,i}$ to be i.i.d. normal random variable with mean $0$ and variance $\sigma^2$. The noise $w_{k,i}$ is chosen such that
\begin{equation}
  w_{k,i} = \begin{cases}
    v_{0,i}&\text{, if }k = 0\\
    \varphi^kv_{k,i}-\varphi^{k-1}v_{k-1,i}&\text{, otherwise}
  \end{cases},
  \label{eq:addednoise}
\end{equation}
where $0<\varphi<1$ is a constant for all agents. Notice that
\begin{align*}
  \sum_{t=0}^k w_{k,i} = \varphi^kv_{k,i}.
\end{align*}
Hence, the noise added by the agent $i$ sums to $0$ asymptotically, which is crucial to ensure the exact average consensus, which is proved by the following theorem:
\begin{theorem}
  Suppose that the matrix $A$ is symmetric with eigenvalues $1=\lambda_1 > \lambda_2 \geq \cdots\geq \lambda_n > -1$. For any initial condition $x_0$, $x_k$ converges to $\bar x$ in the mean square sense. Furthermore, the mean square convergence rate $\rho$ equals
  \begin{equation}
    \rho = \max(\varphi^2,|\lambda_2|^2,|\lambda_n|^2),
    \label{eq:convergencerate}
  \end{equation}
  where $\rho$ is defined as
    \begin{equation}
  \rho \triangleq \lim_{k\rightarrow\infty} \left( \sup_{z_0\neq 0}\frac{\mathbb E \|z_k\|_2^2}{\|z_0\|_2^2}\right)^{1/k},
  \label{eq:convergenceratedef}
\end{equation}
with $z_k = x_k-\bar x$.
  \label{theorem:convergence}
\end{theorem}
\begin{proof}
  Since the RHS of \eqref{eq:convergencerate} is strictly less than 1, we only need to prove \eqref{eq:convergencerate}, since it implies the mean square convergence. By \eqref{eq:matrixform},
  \begin{align*}
    &x_k = A^k x_0 + \sum_{t=0}^{k-1} A^{k-t}w_t = A^kx_{0}  + A\varphi^{k-1}v_{k-1}+ \sum_{t=0}^{k-2}\varphi^tA^{k-t-1}(A-I)v_{t}.
  \end{align*}

 Define matrix $\mathcal A$ to be
  \begin{displaymath}
    \mathcal A\triangleq A - \mathbf 1\mathbf 1^T/n.
  \end{displaymath}
  The following equalities hold for all $k\geq 0$
  \begin{align}
    A^k(A-I) &= \mathcal A^k(A-I),\label{eq:mathcalA1}\\
    A^k-\mathbf 1\mathbf 1^T/n &= \mathcal A^k (I - \mathbf 1\mathbf 1^T/n).\label{eq:mathcalA2}
  \end{align}

  Since $\bar x =(\mathbf 1 \mathbf 1^T/n )x_{0}$, we have
  \begin{displaymath}
    z_{k} = \mathcal A^kz_{0} + A\varphi^{k-1}v_{k-1} + \sum_{t=0}^{k-2}\varphi^t\mathcal A^{k-t-1}(A-I)v_{t}.
  \end{displaymath}
  Since $\{v_{k}\}$ are i.i.d. Gaussian vectors with zero mean and covariance $\sigma^2I$, the mean square error can be written as
  \begin{equation}
    \begin{split}
      \mathbb E z_{k}^Tz_{k}& = z_{0}^T\mathcal A^{2k}z_{0} + \sigma^2\tr(A^2) \varphi^{2k-2} 
      +\sigma^2\sum_{t=0}^{k-2}\varphi^{2t}\tr\left[\mathcal A^{2k-2t-2}(A-I)^2)\right].
    \end{split}
    \label{eq:meansquareerror}
  \end{equation}
  Since all the terms on the RHS of \eqref{eq:meansquareerror} are non-negative,
  \begin{align*}
    \mathbb E z_{k}^Tz_{k}\geq z_{0}^T\mathcal A^{2k}z_{0}, \mathbb E z_{k}^Tz_{k}\geq \sigma^2\tr(A^2) \varphi^{2k-2},
  \end{align*}
  which implies that
  \begin{displaymath}
    \rho \geq \max(\varphi^2,|\lambda_2|^2,|\lambda_n|^2).
  \end{displaymath}
  On the other hand, since the eigenvalues of $A$ are $\lambda_1,\dots,\lambda_n$, we have
  \begin{align*}
    &\;\;\sum_{t=0}^{k-2}\varphi^{2t}\tr\left[\mathcal A^{2k-2t-2}(A-I)^2)\right] 
    =\sum_{i=2}^n\sum_{t=0}^{k-2}\left(\varphi^{2t}\lambda_i^{2k-2t-2}\right)(\lambda_i-1)^2\\
    &\leq (n-1)(k-1)\left[\max(\varphi, |\lambda_2|,|\lambda_n|)\right]^{2k-2} (\lambda_n-1)^2
  \end{align*}
  The last inequality is true due to the fact that for all $t$,
  \begin{displaymath}
    \left(\varphi^{2t}\lambda_i^{2k-2t-2}\right)(\lambda_i-1)^2\leq \left[\max(\varphi, |\lambda_2|,|\lambda_n|)\right]^{2k-2} (\lambda_n-1)^2.
  \end{displaymath}
  Combining with \eqref{eq:meansquareerror}, we can prove that
  \begin{displaymath}
    \rho \leq \max(\varphi^2,|\lambda_2|^2,|\lambda_n|^2).
  \end{displaymath}
  which finishes the proof.
\end{proof}

Next we consider the privacy guarantees provided by the proposed algorithm. Without loss of generality, we consider the case where agent $n$ wants to infer the other agents initial conditions. Suppose agent $n$ has $m$ neighbors, the set of which is denoted as
\begin{displaymath}
  \mathcal N(n) = \{j_1,\dots,j_m\}.
\end{displaymath}
Define
\begin{equation}
  C \triangleq \begin{bmatrix}
    e_{j_1}&\dots&e_{j_m}&e_n
  \end{bmatrix}^T \in \mathbb R^{(m+1)\times n},
\end{equation}
where $e_i$ denotes the $i$th canonical basis vector in $\mathbb R^n$. The information available to the agent $n$ at time $k$ contains its initial state and the messages it sends and it receives up to time $k$, i.e.,
\begin{equation}
  \mathcal I_k \triangleq \{x_{0,n},y_0,\dots,y_k),\ldots\},
  \label{eq:informationset}
\end{equation}
where
\begin{equation}
  y_k \triangleq Cx^+_k= C(x_k+w_k). 
  \label{eq:observation}
\end{equation}

Before continuing on, let us define $\tilde A \in \mathbb R^{(n-1)\times (n-1)}$($\tC \in\mathbb R^{(m-1)\times(n-1)}$), as a submatrix of $A$ ($C$) by removing the last row and column. Define the following matrices:
\begin{align}
  \mathcal U &\triangleq \tC^T\tC \in \mathbb R^{(n-1)\times(n-1)},\, \mathcal V \triangleq I - \mathcal U. \label{eq:projmatrix}
\end{align}

Further denote the eigenvectors of the symmetric matrix $(I-\tA)^{-1}\mathcal U(I-\tA)^{-1}$ as $\psi_1,\dots,\psi_{n-1}\in \mathbb R^{n-1}$. Without loss of generality, we assume that $\{\psi_1,\dots,\psi_{n-1}\}$ forms an orthonormal basis of $\mathbb R^{n-1}$. Furthermore, we assume that the eigenvalues corresponding to the eigenvectors $\{\psi_1,\dots,\psi_{m}\}$ are non-zero and the eigenvalues corresponding to $\{\psi_{m+1},\dots,\psi_{n-1}\}$ are zero. Define the matrix
\begin{align}
  \mathcal Q &\triangleq \begin{bmatrix}
    \psi_{m+1}&\dots&\psi_{n-1}
  \end{bmatrix}.
\end{align}

The following theorem provide privacy guarantees for the proposed algorithm:
\begin{theorem}
  Suppose that $A$ is symmetric. The variance of any unbiased estimate of $x_{0,i}$ given the information set $\mathcal I_k$ is lower bounded by $P_{ii}$, where $P_{ii}$ is the $i$th diagonal entry of $P$ given by the following equality:
  \begin{equation}
    P = \sigma^2\begin{bmatrix}
      \mathcal Q\left[\mathcal Q^T(I-\tA)^{-1}Y(I-\tA)^{-1}\mathcal Q\right]^{-1} \mathcal Q^T&0\\
      0&0
    \end{bmatrix},
    \label{eq:tPeq}
  \end{equation}
 where $ Y = \lim_{k\rightarrow\infty} Y_k$ is the limit of the following recursive Riccati equations:
  \begin{align}
    Y_0& = \tA \mathcal U \tA,\\
    Y_{k+1} &=\tA \mathcal U\tA \label{eq:riccati2} +\varphi^{-2}\tA\left[ Y^+_{k} -  Y^+_{k}\left(\varphi^2 I+Y^+_{k} \right)^{-1}Y^+_{k}\right]\tA,\nonumber
  \end{align}
  where
  \begin{equation}
    Y^+_{k} = \mathcal V Y_{k} \mathcal V \label{eq:riccati1}.
  \end{equation}
  \label{theorem:tPineq}
\end{theorem}
\begin{proof}
  Notice that since the noise are Gaussian, the maximum likelihood estimator of $x_{0}$ given $\mathcal I_{k}$ is the unbiased estimator with the smallest covariance, which is denoted as $P_{k}$.

  It is easy to see since agent $n$ receives more information as $k$ increases, $P_{k}$ is monotonically non-increasing. We then need to establish that $P_{k}$ converges to $P$, which is quite technical. The readers can refer to \cite{Mo2017} for the full details of the proof.
\end{proof}

Notice that as long as $P_{ii}$ is non-zero, we can choose large enough $\sigma^2$ to achieve desired level of privacy. As a result, we need to derive conditions on whether $P_{ii}$ is non zero or not. To this end, let us define the essential neighborhood ${\mathcal N_e}(i)$ of an agent $i$ to be the set of neighboring agents whose information is used to compute \eqref{eq:singleupdate}, i.e.,
\begin{equation}
  {\mathcal N_e}(i) \triangleq \{j\in \mathcal N(i):a_{ij}\neq 0\}.
\end{equation}

A agent $j$ is called as super-neighbor of $i$ if it is a neighbor of $i$ and all $i$'s essential neighbors.

\begin{theorem}
   Agent $n$ can asymptotically infer $x_{0,i}$ with $0$ variance if and only if $n$ is a super-neighbor of $i$.
\end{theorem}

For the detailed proof of the theorem, please refer to \cite{Mo2017}. Notice that the condition is local in the sense that the privacy of the initial condition of agent $i$ can only be breached by its neighbors.

If we compare the differential privacy based approach and inference privacy based approach, we see that differential privacy provides much stronger privacy guarantees, due to the following reasons:
\begin{enumerate}
\item Inference privacy assume that the agent can only access the messages from itself and its neighbors $y(k)$, while differential privacy does not.
\item To prevent privacy breach, inference privacy requires that no super neighbors exists. However, it is not required by differential privacy.
\end{enumerate}

However, inference privacy mechanism can achieve exact average consensus, which is its main advantage over differential privacy mechanism.

It is also worth noticing the other inference privacy metrics exist, e.g., $\epsilon$-information privacy, $\epsilon$-average information leakage. For more detailed discussion, please refer to \cite{Sun2017}.

\section{Cryptography Based Privacy}


These requirements can be satisfied by Paillier cryptosystem.

In a distributed system with multiple agents, secure multi-party computation can be used to preserve the privacy of the agents. Suppose there are $n$ agents, each with a value $x_i$, and the goal is to compute certain function $f(x_1,\ldots,x_n)$.

Informally speaking, secure multi-party computation ensure that the only information about the private data $x_1,\ldots, x_n$ can be inferred from the messages sent during the execution of the protocol is whatever could be inferred from seeing the output of the function alone.

There are multiple ways to achieve secure multi-party computation. For example, oblivious transfer can be used for 2 party computation cite. For multiparty, secret sharing and homomorphic encryption can be adopted cite. The detailed discussion of these approaches is out of the scope of this book.


\bibliographystyle{IEEEtran}
\bibliography{ncs}
\end{document}

%%% Local Variables:
%%% TeX-command-default: "Latexmk"
%%% End:
\subsection{Homomorphic Encryption}
In this section, we consider using partial homomorphic encryption to achieve privacy preserving average consensus. We consider a public key cryptosystem. To be specific, for each agent $i$, there exists a public encryption function $\mathcal E_i$ and a private decryption function $\mathcal D_i$, such that for any fixed-point float $m$:
\begin{align*}
  \mathcal D_i(\mathcal E_i(m)) = m.
\end{align*}

Furthermore, there exists operation $\oplus,\otimes$, such that summation of ciphertext and multiplication by plaintext are supported:
\begin{align*}
  \mathcal E_i(m_1)\oplus \mathcal E_i(m_2) = \mathcal E_i(m_1+m_2),\,m_1\otimes \mathcal E_i(m_2) = \mathcal E_i(m_1m_2).
\end{align*}


Assuming the underlying graph is undirected, we can use the following algorithm to achieve consensus:
\begin{algorithm}
\SetAlgoLined
  \begin{enumerate}
  \item At time $k$, each agent $i$ generates i.i.d. random fixed point float $a_{k,i}$. 
  \item Agent $i$ encrypts the negative of its own state $-x_{k,i}$ using its own encryption function $\mathcal E_i$ and broadcast $\mathcal E_i(-x_{k,i})$.
  \item Upon receiving the encrypted $\mathcal E_j(-x_{k,j})$ from agent $j$, agent $i$ computes the following
    \begin{align*}
      a_{k,i}\otimes\left( \mathcal E_j(x_{k,i}) \oplus \mathcal E_j(-x_{k,j})\right) = \mathcal E_j\left(a_{k,i}(x_{k,i}-x_{k,j})\right),
    \end{align*}
    which is sent back to agent $j$.
  \item After receiving $\mathcal E_i\left(a_{k,j}(x_{k,j}-x_{k,i})\right)$ from agent $j$, agent $i$ decrypts 
    \begin{align*}
a_{k,j}(x_{k,j}-x_{k,i}) = \mathcal D_i\left(\mathcal E_i\left(a_{k,j}(x_{k,j}-x_{k,i})\right)\right)
    \end{align*}
  \item The agent update its state by the following equation:
    \begin{equation}
      x_{k+1,i} =x_{k,i} +\epsilon a_{k,i}\sum_{j\in\mathcal N(i)}a_{k,j}(x_{k,j}-x_{k,i}).
      \label{eq:consensusstephomo}
    \end{equation}
  \item Advance the time to $k+1$ and go to step 1).
  \end{enumerate}
  \label{alg:consensushomo}
 \caption{Average Consensus via Homomorphic Encryption}
\end{algorithm}

Let us define the weighted Laplacian matrix $L_k$ such that
\begin{align*}
  \begin{bmatrix}
L_k
  \end{bmatrix}_{i,j} = \begin{cases}
-a_{k,i}a_{k,j}&\text{if }i\neq j\\
a_{k,i}\sum_{l\in \mathcal N_i}a_{k,l}&\text{if }i= j
  \end{cases}.
\end{align*}

Therefore, \eqref{eq:consensusstephomo} can be written in the matrix form as
\begin{align}
  x_{k+1} = (I - \epsilon L_k)x_{k}.
\end{align}

Notice that agent $i$ only knows $a_{k,j}(x_{k,j}-x_{k,i})$, where both $a_{k,j}$ and $x_{k,j}$ are unknown. Therefore, agent $i$ cannot deduce $x_{k,j}$ simply from the message it receives from agent $j$ at time $k$. Based on this observation, Ruan et al. cite proves the following theorem:
\begin{theorem}
Suppose agent $i$ cannot infer the initial state of agent $j$, if agent $i$ is not the only neighbor of agent $j$.
\label{theorem:topology2}
\end{theorem}

Notice the condition in Theorem~\ref{theorem:topology2} is weaker than the super-neighbor conditions used in Theorem~\ref{theorem:topology}, which shows the power of encryption based approach.

The following theorem characterize the convergence of the proposed consensus algorithm:
\begin{theorem}
  For the consensus algorithm~\ref{alg:consensushomo}, $x_k$ converges to $\bar x$ in the mean square sense, if the following matrix is strictly stable:
  \begin{align*}
  \Sigma = \mathbb E (I-\epsilon L_k-\mathbf 1\mathbf 1^T/n)\bigotimes (I-\epsilon L_k-\mathbf 1\mathbf 1^T/n),
  \end{align*}
  where $\bigotimes$ is the Kronecker product.

Furthermore, the mean square convergence rate $\rho$, which is defined in \ref{eq:convergenceratedef}, is given by the spectral radius of $\Sigma$.
\end{theorem}
\begin{proof}
  Notice that
  \begin{align*}
z_{k+1}z_{k+1}^T = (I - \epsilon L_k-\mathbf 1\mathbf 1^T/n)z_{k}z_k^T(I - \epsilon L_k-\mathbf 1\mathbf 1^T/n).
  \end{align*}
  Therefore
  \begin{align*}
    \vect(z_{k+1}z_{k+1}^T) &= (I-\epsilon L_k-\mathbf 1\mathbf 1^T/n)\bigotimes (I-\epsilon L_k-\mathbf 1\mathbf 1^T/n)\vect(z_{k}z_{k}^T)\\
    &=\prod_{t=0}^k\left((I-\epsilon L_t-\mathbf 1\mathbf 1^T/n)\bigotimes (I-\epsilon L_t-\mathbf 1\mathbf 1^T/n)\right)\vect(z_{0}z_{0}^T),
  \end{align*}
  where $\vect(X)$ is the verctorization of matrix $X$. Take expected value on both side and use the fact that $a_{k,i}$ are i.i.d. distributed, we get
  \begin{align*}
   \mathbb E \vect(z_{k}z_{k}^T) =\Sigma^{k}\vect(z_{0}z_{0}^T).
  \end{align*}
  One can prove that
  \begin{align*}
    \|\vect(z_{k}z_{k}^T)\|_1\leq  \|z_k\|_2^2 \leq n\|\vect(z_{k}z_{k}^T)\|_\infty.
%  \end{align*}
%  Hence, if $\Sigma$ is stable, then $\mathbb E\|z_k\|_2^2$ converges to $0$. Furthermore, the mean square convergence rate $\rho$ is no greater than the spectral radius of $\Sigma$.
%
%  Next we want to prove that $\rho$ is no less than the spectral radius of $\Sigma$. Without loss of generality, we can assume that the spectral radius is non-zero. One can prove that $\Sigma$ is symmetric. Hence its eigenvalues and eigenvectors are real. Suppose $\vect(X)$ is an eigenvector of $\Sigma$, it can be proved that $\vect(X^T)$ is also an eigenvector corresponding to the same eigenvalues. 
%  
%  As a result, let $\lambda$ be the eigenvalue of $\Sigma$ with the largest absolute value, we can assume that $\vect(S)$ is the eigenvector corresponding to $\lambda_1$ and $S$ is symmetric. Therefore, $S$ can be decomposed into 
%\begin{align*}
%  S=\sum_{i=1}^n\alpha_i\xi_i\xi_i^T,
%\end{align*}
%where $\{\xi_i\}$ is an orthogonal basis set and $\alpha_i = 0$ or $\pm 1$. As a result,
%\begin{align*}
%  \vect(S) = \lambda^{-k}\Sigma^k \vect(S), 
%\end{align*}
%which implies that there must exists an $\xi_i$, such that
%\begin{align*}
%\limsup_{k\rightarrow\infty}  \|\lambda^{-k}\Sigma^k \vect(\xi_i\xi_i^T)  \|_1 \geq \frac{1}{n}\|\vect(S)\|_1.
%\end{align*}
%Let $\zeta_i = (I-\mathbf 1\mathbf 1^T/n)\xi_i$, then $\Sigma \vect(\zeta_i\zeta_i^T) = \Sigma \vect(\xi_i\xi_i^T)$. As a result, we can prove $\rho \geq |\lambda|$ by choosing $z_0 =\zeta_i$, which finishes the proof.
%\end{proof}
%
